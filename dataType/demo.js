// 1. boolean
var isDone = false;
// 2. Number
var decimal = 12;
var hex = 0xf00d;
var binary = 10;
var octal = 484;
// 3. String
var username = 'phongdat';
// 4. Array
var students = ['Cuong', 'Kinh', 'dat'];
var students_2 = ['Cuong', 'Kinh', 'Chinh'];
// 5. Tuple
var students_3 = ['Cuong', 27];
// 6. Enum
//enum Fruits {Orange, Banana, Mango, Apple}
var Fruits;
(function (Fruits) {
    Fruits[Fruits["Orange"] = 12] = "Orange";
    Fruits[Fruits["Banana"] = 13] = "Banana";
    Fruits[Fruits["Mango"] = 14] = "Mango";
    Fruits[Fruits["Apple"] = 15] = "Apple";
})(Fruits || (Fruits = {}));
var a = Fruits.Orange;
alert(a);
// 7.Any
var notSure = 4; // kiểu number
notSure = "Thay thế bằng kiểu string";
notSure = false; // bây giờ lại là kiểu boolean
var list = [1, true, "free"];
// 8.Void . kieu du lieu voi gia tri la null,
function showMessage() {
    alert("Success!");
}
var unusable = undefined;
// 9. Null và Undefined
var u = undefined;
var n = null;

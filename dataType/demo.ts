// 1. boolean
let isDone: boolean = false;
// 2. Number
var decimal: number = 12;
var hex: number = 0xf00d;
var binary: number = 0b1010;
var octal: number = 0o744;
// 3. String
let username: string = 'phongdat';
// 4. Array
let students: string[] = ['Cuong', 'Kinh', 'dat'];
let students_2: Array<string> = ['Cuong', 'Kinh', 'Chinh'];
// 5. Tuple
let students_3: [string, number] = ['Cuong', 27];
// 6. Enum
//enum Fruits {Orange, Banana, Mango, Apple}
enum Fruits {Orange = 12, Banana, Mango, Apple}
let a: Fruits = Fruits.Orange;
alert(a);
// 7.Any
let notSure: any = 4; // kiểu number
notSure = "Thay thế bằng kiểu string";
notSure = false; // bây giờ lại là kiểu boolean
let list: any[] = [1, true, "free"];
// 8.Void . kieu du lieu voi gia tri la null,
function showMessage(): void {
    alert("Success!");
}

let unusable: void = undefined;
// 9. Null và Undefined
let u: undefined = undefined;
let n: null = null;

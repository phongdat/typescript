var list = [4, 5, 6];
//for in => i la key
for (var i_1 in list) {
    console.log(i_1); // "0", "1", "2",
}
//for of =? i la value
for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
    var i_2 = list_1[_i];
    console.log(i_2); // "4", "5", "6"
}
//for
for (var j = 0; j <= 10; j++) {
    console.log(j);
}
//while
var n = 10;
while (n <= 1000) {
    console.log(n);
    n++;
}
//do ... while
var i = 10;
do {
    console.log(i);
    i++;
} while (i <= 1000);

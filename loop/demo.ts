let list = [4, 5, 6];
//for in => i la key
for (let i in list) {
    console.log(i); // "0", "1", "2",
}
//for of =? i la value
for (let i of list) {
    console.log(i); // "4", "5", "6"
}
//for
for (let j = 0; j <= 10; j++)
{
    console.log(j)
}
//while
var n = 10;
while (n <= 1000) {
    console.log(n);
    n++;
}
//do ... while
var i = 10;
do {
    console.log(i);
    i++;
} while (i <= 1000);
